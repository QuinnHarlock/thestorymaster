﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    [SerializeField] private float enemyDamage;
    [SerializeField] private HealthController healthController;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Sword"))//if enemy collider detects said tag
        {
            Damage();
        }

    }

    
    void Damage()
    {
        healthController.playerHealth = healthController.playerHealth - enemyDamage;
        healthController.UpdateHealth();
        this.gameObject.SetActive(false);
    }

}