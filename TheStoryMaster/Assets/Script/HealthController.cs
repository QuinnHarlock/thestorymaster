﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public float playerHealth;
    [SerializeField] private Text healthText;


    private void Start()
    {
        UpdateHealth();//Update health on start to check if its been changed from default
    }


    public void UpdateHealth()
    {
        healthText.text = playerHealth.ToString("0");//Update player health on canvas
    }

}
