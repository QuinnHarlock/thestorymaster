﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileThrow : MonoBehaviour
{
    public GameObject knife;
    public Transform spawnPoint;
    public float speed = 5f;

    bool launched;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            launched = true;
    }
    private void FixedUpdate()
    {
        if (launched)
        {
            ThrowKnife();
        }
    }

    private void ThrowKnife()
    {
        GameObject knifeInstance = Instantiate(knife, spawnPoint.position, knife.transform.rotation);
        knifeInstance.transform.rotation = Quaternion.LookRotation(-spawnPoint.forward);
        Rigidbody knifeRig = knifeInstance.GetComponent<Rigidbody>();

        knifeRig.AddForce(spawnPoint.forward * speed, ForceMode.Impulse);
        launched = false;
    }

}
