﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;

    public Rigidbody2D rb;

    private Vector2 moveDirection;

    public Animator animator;


    void Update()
    {
       ProcessInputs();//Process movements input

       animator.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
       animator.SetFloat("Vertical", Input.GetAxis("Vertical"));
    }


    private void FixedUpdate()
    {
       Move();
    }


    void ProcessInputs()
    {
       float moveX = Input.GetAxisRaw("Horizontal");
       float moveY = Input.GetAxisRaw("Vertical");

       moveDirection = new Vector2(moveX, moveY).normalized;
    }


    private void Move()
    {
        rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);

    }

}