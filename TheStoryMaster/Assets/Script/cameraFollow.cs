﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    public GameObject player;
    public Transform transform;

    private Vector3 offset; //Variable to store offset between player and cam.

    void Start()
    {
        offset = transform.position - player.transform.position;
        //print(transform.rotation);
    }

    private void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }


}
